﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen2TRIM
{
    public partial class FormDatos : Form
    {
        //Constructor
        public FormDatos()
        {
            InitializeComponent();
            rellenarListBox();
        }

        //Nos rellenará el listBox con todos los códigos
        private void rellenarListBox()
        {
            String[] codigos = { "MEM400","MEM200","MEM100","HDD100","HDD002","HDD003","PRO125","PRO348","PRO100","IMP386","IMP678","IMP567","SCN448","SCN586","SCN689"};
            codLB.Items.AddRange(codigos);
        }

        //----------------------------
        //BOTONES DEL FORMULARIO
        //----------------------------
        
        //Botón añadir
        private void AnnadirBTN_Click(object sender, EventArgs e)
        {
            if(codTB.TextLength > 0 && precioTB.TextLength > 0)
            {
                if (validarCodigo())
                {
                    //Nos aseguramos el no meter el mismo codigo 2 veces
                    if (!comprobarCodigoRepetido(codTB.Text))
                    {
                        codLB.Items.Add(codTB.Text);
                    }
                    else
                    {
                        MessageBox.Show("No se puede insertar de nuevo el código");
                    }
                    
                }
                else
                {
                    MessageBox.Show("No está correcto el formato");
                }
            }
            else
            {
                MessageBox.Show("Debe insertar primero los datos");
            }
        }//Fin botón añadir


        //Botón eliminar
        //Tendremos que hacerlo en 2 bucles, porque sino nos dará error el índice
        private void elimBTN_Click(object sender, EventArgs e)
        {
            if (codLB.SelectedItems.Count > 0)
            {
                ArrayList codigosSelec = new ArrayList();
                //Para añadirlo
                foreach (String cod in this.codLB.SelectedItems)
                {
                    codigosSelec.Add(cod);
                }

                //Para borrarlo
                foreach (string cod in codigosSelec)
                {
                    this.codLB.Items.Remove(cod);
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar primero algún código");
            }
        }//Fin botón eliminar


        //---------------------------
        //MétodosAuxiliares
        //---------------------------

        //Nos devolvera true si el código está repetido en el listbox y false sino lo está
        private bool comprobarCodigoRepetido(String cod)
        {
            Boolean estaRepetido = false;

            foreach (String codigo in this.codLB.Items)
            {
                if (cod == codigo)
                    estaRepetido = true;
            }
            return estaRepetido;
        }

        //Para validar el código
        private Boolean validarCodigo()
        {
            Boolean todoCorrecto = false;
            Boolean parteLetraCorrecta = false;
            Boolean parteNumCorrecta = false ;

            //Primero nos aseguramos que tenga sólo 6 dígitos
            if(codTB.TextLength == 6)
            {
                //Luego separamos en 2 partes: parte Letras y parte de números
                //nos ayudaremos para ello con un array de chars
                char[] codArray = codTB.Text.ToCharArray();

                String parteLetra = "";
                String parteNum = "";


                //tomamos la primera parte
                for (int i = 0; i < codArray.Length - (codArray.Length / 2); i++)
                {
                    parteLetra += codArray[i];
                }

                //tomamos la segunda parte
                for (int i = codArray.Length / 2; i < codArray.Length; i++)
                {
                    parteNum += codArray[i];
                }

                //Ahora validaremos
                //Primero la parte de las letras
                if (parteLetra == "MEM" || parteLetra == "HDD" || parteLetra == "PRO" || parteLetra == "IMP" || parteLetra == "SCN")
                {
                    parteLetraCorrecta = true;
                }

                //Luego la parte numérica
                if (Convert.ToInt32(parteNum) != null)
                {
                    parteNumCorrecta = true;
                }

                //Nos aseguramos que ambas partes estén correctas
                if(parteNumCorrecta && parteLetraCorrecta)
                {
                    todoCorrecto = true;
                }
                else
                {
                    todoCorrecto = false;
                }
            }
            else
            {
                todoCorrecto = false;
            }
            

            return todoCorrecto;
        }

        

        //Para validar la entrada de números
        private void precioTB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
