﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen2TRIM
{
    public partial class Form1 : Form
    {
        //nuestras ventanas-form hijos
        private FormDatos fd;
        private FormPresupuestos fp;
        //Ventana de acerca de...
        private AboutBox1 ab;
        public Form1()
        {
            this.fd = new FormDatos();
            this.fp = new FormPresupuestos();
            this.ab = new AboutBox1();
            InitializeComponent();
        }

        //-----------------------------
        // MenuStrip
        //-----------------------------
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            salirPrograma();
        }

        //Abre el From-Ventana hija Datos
        private void datosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            abrirFormDatos();
        }

        
        //Abre la ventana hija-Presupuesto
        private void presupuestoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            abrirFormPresupuestos();
        }

        //Menu Acerca de
        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ab.ShowDialog();
        }

        //SALIR
        private void SalirTPB_Click_1(object sender, EventArgs e)
        {
            salirPrograma();
        }


        //------------------------------
        // ToolStripMenu
        //------------------------------
        private void SalirTPB_Click(object sender, EventArgs e)
        {
            salirPrograma();
        }

        private void PresupuestoTPB_Click(object sender, EventArgs e)
        {
            abrirFormPresupuestos();
        }


        //--------------------------------
        //Métodos auxiliares
        //--------------------------------


        private void salirPrograma()
        {
            this.Close();
            Application.Exit();
        }


        private void abrirFormDatos()
        {
            this.fd.MdiParent = this;
            this.fd.WindowState = FormWindowState.Maximized;
            this.fd.Show();
        }


        private void abrirFormPresupuestos()
        {
            this.fp.MdiParent = this;
            this.fp.WindowState = FormWindowState.Maximized;
            this.fp.Show();
        }

        
    }
}
