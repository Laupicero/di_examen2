﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen2TRIM
{
    /// <summary>
    /// Con está clase englobaremos los códigos y sus precios para más comodidad
    /// </summary>
    class Componentes
    {
        private String codigo;
        private int precio;

        //Cosntructor
        public Componentes() { }

        public Componentes(String codigo, int precio) {
            this.Codigo = codigo;
            this.Precio = precio;
        }

        //SETTER Y GETTER
        public string Codigo { get => codigo; set => codigo = value; }
        public int Precio { get => precio; set => precio = value; }
    }
}
