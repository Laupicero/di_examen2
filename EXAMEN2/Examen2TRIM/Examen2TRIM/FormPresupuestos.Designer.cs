﻿
namespace Examen2TRIM
{
    partial class FormPresupuestos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.confgBasicaTC = new System.Windows.Forms.TabControl();
            this.ConfigTB = new System.Windows.Forms.TabPage();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lujoRB = new System.Windows.Forms.RadioButton();
            this.simpleRB = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.color1RB = new System.Windows.Forms.RadioButton();
            this.color14RB = new System.Windows.Forms.RadioButton();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.discoDuroLB = new System.Windows.Forms.ListBox();
            this.memoriaLB = new System.Windows.Forms.ListBox();
            this.precioProcesadorTB = new System.Windows.Forms.TextBox();
            this.preocesadorCB = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AccesoriosTB = new System.Windows.Forms.TabPage();
            this.ScannerPrecioTB = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.impresoraPrecioTB = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.scanChB = new System.Windows.Forms.CheckBox();
            this.impresChB = new System.Windows.Forms.CheckBox();
            this.lectoraCDTB = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lectoraDiscoTB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.x80RB = new System.Windows.Forms.RadioButton();
            this.x60RB = new System.Windows.Forms.RadioButton();
            this.x40RB = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.m3RB = new System.Windows.Forms.RadioButton();
            this.m2RB = new System.Windows.Forms.RadioButton();
            this.m1RB = new System.Windows.Forms.RadioButton();
            this.lectoraCDChB = new System.Windows.Forms.CheckBox();
            this.LectoraDiscoChB = new System.Windows.Forms.CheckBox();
            this.GeneralTB = new System.Windows.Forms.TabPage();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ClienteCB = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tlfTB = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.direccionTB = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.calcBTN = new System.Windows.Forms.Button();
            this.nuevoBTN = new System.Windows.Forms.Button();
            this.confgBasicaTC.SuspendLayout();
            this.ConfigTB.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.AccesoriosTB.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.GeneralTB.SuspendLayout();
            this.SuspendLayout();
            // 
            // confgBasicaTC
            // 
            this.confgBasicaTC.Controls.Add(this.ConfigTB);
            this.confgBasicaTC.Controls.Add(this.AccesoriosTB);
            this.confgBasicaTC.Controls.Add(this.GeneralTB);
            this.confgBasicaTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.confgBasicaTC.Location = new System.Drawing.Point(0, 0);
            this.confgBasicaTC.Name = "confgBasicaTC";
            this.confgBasicaTC.SelectedIndex = 0;
            this.confgBasicaTC.Size = new System.Drawing.Size(800, 450);
            this.confgBasicaTC.TabIndex = 0;
            // 
            // ConfigTB
            // 
            this.ConfigTB.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ConfigTB.Controls.Add(this.textBox5);
            this.ConfigTB.Controls.Add(this.textBox4);
            this.ConfigTB.Controls.Add(this.groupBox2);
            this.ConfigTB.Controls.Add(this.groupBox1);
            this.ConfigTB.Controls.Add(this.textBox3);
            this.ConfigTB.Controls.Add(this.textBox2);
            this.ConfigTB.Controls.Add(this.discoDuroLB);
            this.ConfigTB.Controls.Add(this.memoriaLB);
            this.ConfigTB.Controls.Add(this.precioProcesadorTB);
            this.ConfigTB.Controls.Add(this.preocesadorCB);
            this.ConfigTB.Controls.Add(this.label8);
            this.ConfigTB.Controls.Add(this.label7);
            this.ConfigTB.Controls.Add(this.label6);
            this.ConfigTB.Controls.Add(this.label5);
            this.ConfigTB.Controls.Add(this.label4);
            this.ConfigTB.Controls.Add(this.label3);
            this.ConfigTB.Controls.Add(this.label2);
            this.ConfigTB.Controls.Add(this.label1);
            this.ConfigTB.Location = new System.Drawing.Point(4, 22);
            this.ConfigTB.Name = "ConfigTB";
            this.ConfigTB.Padding = new System.Windows.Forms.Padding(3);
            this.ConfigTB.Size = new System.Drawing.Size(792, 424);
            this.ConfigTB.TabIndex = 0;
            this.ConfigTB.Text = "Configuracion Básica";
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(390, 366);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(153, 20);
            this.textBox5.TabIndex = 17;
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(43, 366);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(153, 20);
            this.textBox4.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lujoRB);
            this.groupBox2.Controls.Add(this.simpleRB);
            this.groupBox2.Location = new System.Drawing.Point(390, 252);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(202, 79);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tipo de Teclado";
            // 
            // lujoRB
            // 
            this.lujoRB.AutoSize = true;
            this.lujoRB.Location = new System.Drawing.Point(31, 46);
            this.lujoRB.Name = "lujoRB";
            this.lujoRB.Size = new System.Drawing.Size(62, 17);
            this.lujoRB.TabIndex = 1;
            this.lujoRB.TabStop = true;
            this.lujoRB.Text = "De Lujo";
            this.lujoRB.UseVisualStyleBackColor = true;
            // 
            // simpleRB
            // 
            this.simpleRB.AutoSize = true;
            this.simpleRB.Location = new System.Drawing.Point(31, 23);
            this.simpleRB.Name = "simpleRB";
            this.simpleRB.Size = new System.Drawing.Size(56, 17);
            this.simpleRB.TabIndex = 0;
            this.simpleRB.TabStop = true;
            this.simpleRB.Text = "Simple";
            this.simpleRB.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.color1RB);
            this.groupBox1.Controls.Add(this.color14RB);
            this.groupBox1.Location = new System.Drawing.Point(43, 252);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(202, 79);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo de Monitor";
            // 
            // color1RB
            // 
            this.color1RB.AutoSize = true;
            this.color1RB.Location = new System.Drawing.Point(24, 42);
            this.color1RB.Name = "color1RB";
            this.color1RB.Size = new System.Drawing.Size(78, 17);
            this.color1RB.TabIndex = 3;
            this.color1RB.TabStop = true;
            this.color1RB.Text = "15\" a Color";
            this.color1RB.UseVisualStyleBackColor = true;
            // 
            // color14RB
            // 
            this.color14RB.AutoSize = true;
            this.color14RB.Location = new System.Drawing.Point(24, 19);
            this.color14RB.Name = "color14RB";
            this.color14RB.Size = new System.Drawing.Size(78, 17);
            this.color14RB.TabIndex = 2;
            this.color14RB.TabStop = true;
            this.color14RB.Text = "14\" a Color";
            this.color14RB.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(390, 217);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(153, 20);
            this.textBox3.TabIndex = 13;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(43, 217);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(153, 20);
            this.textBox2.TabIndex = 12;
            // 
            // discoDuroLB
            // 
            this.discoDuroLB.FormattingEnabled = true;
            this.discoDuroLB.Location = new System.Drawing.Point(390, 90);
            this.discoDuroLB.Name = "discoDuroLB";
            this.discoDuroLB.Size = new System.Drawing.Size(169, 95);
            this.discoDuroLB.TabIndex = 11;
            // 
            // memoriaLB
            // 
            this.memoriaLB.FormattingEnabled = true;
            this.memoriaLB.Location = new System.Drawing.Point(43, 90);
            this.memoriaLB.Name = "memoriaLB";
            this.memoriaLB.Size = new System.Drawing.Size(169, 95);
            this.memoriaLB.TabIndex = 10;
            // 
            // precioProcesadorTB
            // 
            this.precioProcesadorTB.Enabled = false;
            this.precioProcesadorTB.Location = new System.Drawing.Point(390, 38);
            this.precioProcesadorTB.Name = "precioProcesadorTB";
            this.precioProcesadorTB.Size = new System.Drawing.Size(153, 20);
            this.precioProcesadorTB.TabIndex = 9;
            // 
            // preocesadorCB
            // 
            this.preocesadorCB.FormattingEnabled = true;
            this.preocesadorCB.Location = new System.Drawing.Point(43, 38);
            this.preocesadorCB.Name = "preocesadorCB";
            this.preocesadorCB.Size = new System.Drawing.Size(169, 21);
            this.preocesadorCB.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(387, 350);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Precio Teclado:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(42, 350);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Precio Monitor:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(390, 201);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Precio Disco Duro:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Precio Memoria";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(387, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Disco Duro";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Memoria";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(387, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Precío Procesador:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo de Procesador:";
            // 
            // AccesoriosTB
            // 
            this.AccesoriosTB.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.AccesoriosTB.Controls.Add(this.ScannerPrecioTB);
            this.AccesoriosTB.Controls.Add(this.label12);
            this.AccesoriosTB.Controls.Add(this.impresoraPrecioTB);
            this.AccesoriosTB.Controls.Add(this.label11);
            this.AccesoriosTB.Controls.Add(this.listBox2);
            this.AccesoriosTB.Controls.Add(this.listBox1);
            this.AccesoriosTB.Controls.Add(this.scanChB);
            this.AccesoriosTB.Controls.Add(this.impresChB);
            this.AccesoriosTB.Controls.Add(this.lectoraCDTB);
            this.AccesoriosTB.Controls.Add(this.label10);
            this.AccesoriosTB.Controls.Add(this.lectoraDiscoTB);
            this.AccesoriosTB.Controls.Add(this.label9);
            this.AccesoriosTB.Controls.Add(this.groupBox4);
            this.AccesoriosTB.Controls.Add(this.groupBox3);
            this.AccesoriosTB.Controls.Add(this.lectoraCDChB);
            this.AccesoriosTB.Controls.Add(this.LectoraDiscoChB);
            this.AccesoriosTB.Location = new System.Drawing.Point(4, 22);
            this.AccesoriosTB.Name = "AccesoriosTB";
            this.AccesoriosTB.Padding = new System.Windows.Forms.Padding(3);
            this.AccesoriosTB.Size = new System.Drawing.Size(792, 424);
            this.AccesoriosTB.TabIndex = 1;
            this.AccesoriosTB.Text = "Accesorios";
            // 
            // ScannerPrecioTB
            // 
            this.ScannerPrecioTB.Enabled = false;
            this.ScannerPrecioTB.Location = new System.Drawing.Point(428, 364);
            this.ScannerPrecioTB.Name = "ScannerPrecioTB";
            this.ScannerPrecioTB.Size = new System.Drawing.Size(100, 20);
            this.ScannerPrecioTB.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(428, 348);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Precio Scanner:";
            // 
            // impresoraPrecioTB
            // 
            this.impresoraPrecioTB.Enabled = false;
            this.impresoraPrecioTB.Location = new System.Drawing.Point(37, 364);
            this.impresoraPrecioTB.Name = "impresoraPrecioTB";
            this.impresoraPrecioTB.Size = new System.Drawing.Size(100, 20);
            this.impresoraPrecioTB.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(37, 348);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Precio Impresora:";
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(431, 239);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(189, 95);
            this.listBox2.TabIndex = 11;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(37, 239);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(189, 95);
            this.listBox1.TabIndex = 10;
            // 
            // scanChB
            // 
            this.scanChB.AutoSize = true;
            this.scanChB.Location = new System.Drawing.Point(431, 216);
            this.scanChB.Name = "scanChB";
            this.scanChB.Size = new System.Drawing.Size(66, 17);
            this.scanChB.TabIndex = 9;
            this.scanChB.Text = "Scanner";
            this.scanChB.UseVisualStyleBackColor = true;
            // 
            // impresChB
            // 
            this.impresChB.AutoSize = true;
            this.impresChB.Location = new System.Drawing.Point(37, 216);
            this.impresChB.Name = "impresChB";
            this.impresChB.Size = new System.Drawing.Size(72, 17);
            this.impresChB.TabIndex = 8;
            this.impresChB.Text = "Impresora";
            this.impresChB.UseVisualStyleBackColor = true;
            // 
            // lectoraCDTB
            // 
            this.lectoraCDTB.Enabled = false;
            this.lectoraCDTB.Location = new System.Drawing.Point(428, 169);
            this.lectoraCDTB.Name = "lectoraCDTB";
            this.lectoraCDTB.Size = new System.Drawing.Size(100, 20);
            this.lectoraCDTB.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(428, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Precio Lectora CD:";
            // 
            // lectoraDiscoTB
            // 
            this.lectoraDiscoTB.Enabled = false;
            this.lectoraDiscoTB.Location = new System.Drawing.Point(37, 173);
            this.lectoraDiscoTB.Name = "lectoraDiscoTB";
            this.lectoraDiscoTB.Size = new System.Drawing.Size(100, 20);
            this.lectoraDiscoTB.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(37, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Precio Lectora Disco:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.x80RB);
            this.groupBox4.Controls.Add(this.x60RB);
            this.groupBox4.Controls.Add(this.x40RB);
            this.groupBox4.Location = new System.Drawing.Point(431, 40);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(189, 110);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tipo de Lectora";
            // 
            // x80RB
            // 
            this.x80RB.AutoSize = true;
            this.x80RB.Location = new System.Drawing.Point(25, 76);
            this.x80RB.Name = "x80RB";
            this.x80RB.Size = new System.Drawing.Size(47, 17);
            this.x80RB.TabIndex = 2;
            this.x80RB.TabStop = true;
            this.x80RB.Text = "80 X";
            this.x80RB.UseVisualStyleBackColor = true;
            // 
            // x60RB
            // 
            this.x60RB.AutoSize = true;
            this.x60RB.Location = new System.Drawing.Point(25, 53);
            this.x60RB.Name = "x60RB";
            this.x60RB.Size = new System.Drawing.Size(47, 17);
            this.x60RB.TabIndex = 1;
            this.x60RB.TabStop = true;
            this.x60RB.Text = "60 X";
            this.x60RB.UseVisualStyleBackColor = true;
            // 
            // x40RB
            // 
            this.x40RB.AutoSize = true;
            this.x40RB.Location = new System.Drawing.Point(25, 30);
            this.x40RB.Name = "x40RB";
            this.x40RB.Size = new System.Drawing.Size(47, 17);
            this.x40RB.TabIndex = 0;
            this.x40RB.TabStop = true;
            this.x40RB.Text = "40 X";
            this.x40RB.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.m3RB);
            this.groupBox3.Controls.Add(this.m2RB);
            this.groupBox3.Controls.Add(this.m1RB);
            this.groupBox3.Location = new System.Drawing.Point(37, 40);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(189, 110);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tipo de Lectora";
            // 
            // m3RB
            // 
            this.m3RB.AutoSize = true;
            this.m3RB.Location = new System.Drawing.Point(25, 76);
            this.m3RB.Name = "m3RB";
            this.m3RB.Size = new System.Drawing.Size(69, 17);
            this.m3RB.TabIndex = 2;
            this.m3RB.TabStop = true;
            this.m3RB.Text = "Modelo 3";
            this.m3RB.UseVisualStyleBackColor = true;
            // 
            // m2RB
            // 
            this.m2RB.AutoSize = true;
            this.m2RB.Location = new System.Drawing.Point(25, 53);
            this.m2RB.Name = "m2RB";
            this.m2RB.Size = new System.Drawing.Size(69, 17);
            this.m2RB.TabIndex = 1;
            this.m2RB.TabStop = true;
            this.m2RB.Text = "Modelo 2";
            this.m2RB.UseVisualStyleBackColor = true;
            // 
            // m1RB
            // 
            this.m1RB.AutoSize = true;
            this.m1RB.Location = new System.Drawing.Point(25, 30);
            this.m1RB.Name = "m1RB";
            this.m1RB.Size = new System.Drawing.Size(69, 17);
            this.m1RB.TabIndex = 0;
            this.m1RB.TabStop = true;
            this.m1RB.Text = "Modelo 1";
            this.m1RB.UseVisualStyleBackColor = true;
            // 
            // lectoraCDChB
            // 
            this.lectoraCDChB.AutoSize = true;
            this.lectoraCDChB.Location = new System.Drawing.Point(431, 16);
            this.lectoraCDChB.Name = "lectoraCDChB";
            this.lectoraCDChB.Size = new System.Drawing.Size(123, 17);
            this.lectoraCDChB.TabIndex = 1;
            this.lectoraCDChB.Text = "Lectora de CD ROM";
            this.lectoraCDChB.UseVisualStyleBackColor = true;
            // 
            // LectoraDiscoChB
            // 
            this.LectoraDiscoChB.AutoSize = true;
            this.LectoraDiscoChB.Location = new System.Drawing.Point(37, 16);
            this.LectoraDiscoChB.Name = "LectoraDiscoChB";
            this.LectoraDiscoChB.Size = new System.Drawing.Size(107, 17);
            this.LectoraDiscoChB.TabIndex = 0;
            this.LectoraDiscoChB.Text = "Lectora de Disco";
            this.LectoraDiscoChB.UseVisualStyleBackColor = true;
            // 
            // GeneralTB
            // 
            this.GeneralTB.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GeneralTB.Controls.Add(this.nuevoBTN);
            this.GeneralTB.Controls.Add(this.calcBTN);
            this.GeneralTB.Controls.Add(this.label19);
            this.GeneralTB.Controls.Add(this.textBox8);
            this.GeneralTB.Controls.Add(this.label20);
            this.GeneralTB.Controls.Add(this.textBox9);
            this.GeneralTB.Controls.Add(this.label21);
            this.GeneralTB.Controls.Add(this.textBox10);
            this.GeneralTB.Controls.Add(this.label18);
            this.GeneralTB.Controls.Add(this.textBox7);
            this.GeneralTB.Controls.Add(this.label17);
            this.GeneralTB.Controls.Add(this.textBox6);
            this.GeneralTB.Controls.Add(this.label16);
            this.GeneralTB.Controls.Add(this.textBox1);
            this.GeneralTB.Controls.Add(this.direccionTB);
            this.GeneralTB.Controls.Add(this.label15);
            this.GeneralTB.Controls.Add(this.tlfTB);
            this.GeneralTB.Controls.Add(this.label14);
            this.GeneralTB.Controls.Add(this.ClienteCB);
            this.GeneralTB.Controls.Add(this.label13);
            this.GeneralTB.Location = new System.Drawing.Point(4, 22);
            this.GeneralTB.Name = "GeneralTB";
            this.GeneralTB.Padding = new System.Windows.Forms.Padding(3);
            this.GeneralTB.Size = new System.Drawing.Size(792, 424);
            this.GeneralTB.TabIndex = 2;
            this.GeneralTB.Text = "General";
            // 
            // ClienteCB
            // 
            this.ClienteCB.FormattingEnabled = true;
            this.ClienteCB.Location = new System.Drawing.Point(31, 34);
            this.ClienteCB.Name = "ClienteCB";
            this.ClienteCB.Size = new System.Drawing.Size(169, 21);
            this.ClienteCB.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(28, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Nombre del cliente:";
            // 
            // tlfTB
            // 
            this.tlfTB.Enabled = false;
            this.tlfTB.Location = new System.Drawing.Point(284, 33);
            this.tlfTB.Name = "tlfTB";
            this.tlfTB.Size = new System.Drawing.Size(163, 20);
            this.tlfTB.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(284, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "Teléfono:";
            // 
            // direccionTB
            // 
            this.direccionTB.Enabled = false;
            this.direccionTB.Location = new System.Drawing.Point(28, 94);
            this.direccionTB.Name = "direccionTB";
            this.direccionTB.Size = new System.Drawing.Size(419, 20);
            this.direccionTB.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(28, 78);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "Dirección:";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(284, 141);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(163, 20);
            this.textBox1.TabIndex = 15;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(51, 148);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(178, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Precio Total / Configuración Básica:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(51, 174);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(137, 13);
            this.label17.TabIndex = 18;
            this.label17.Text = "Precio Total de Accesorios:";
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(284, 167);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(163, 20);
            this.textBox6.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(51, 200);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 13);
            this.label18.TabIndex = 20;
            this.label18.Text = "Precio total de Venta:";
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(284, 193);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(163, 20);
            this.textBox7.TabIndex = 19;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(51, 278);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(104, 13);
            this.label19.TabIndex = 26;
            this.label19.Text = "Precio total A Pagar:";
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(284, 271);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(163, 20);
            this.textBox8.TabIndex = 25;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(51, 252);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "Total I.V.A. ( 21%)";
            // 
            // textBox9
            // 
            this.textBox9.Enabled = false;
            this.textBox9.Location = new System.Drawing.Point(284, 245);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(163, 20);
            this.textBox9.TabIndex = 23;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(51, 226);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "Descuento (%):";
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(284, 219);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(163, 20);
            this.textBox10.TabIndex = 21;
            // 
            // calcBTN
            // 
            this.calcBTN.Location = new System.Drawing.Point(54, 316);
            this.calcBTN.Name = "calcBTN";
            this.calcBTN.Size = new System.Drawing.Size(123, 32);
            this.calcBTN.TabIndex = 27;
            this.calcBTN.Text = "CALCULAR";
            this.calcBTN.UseVisualStyleBackColor = true;
            this.calcBTN.Click += new System.EventHandler(this.calcBTN_Click);
            // 
            // nuevoBTN
            // 
            this.nuevoBTN.Location = new System.Drawing.Point(284, 316);
            this.nuevoBTN.Name = "nuevoBTN";
            this.nuevoBTN.Size = new System.Drawing.Size(123, 32);
            this.nuevoBTN.TabIndex = 28;
            this.nuevoBTN.Text = "NUEVO";
            this.nuevoBTN.UseVisualStyleBackColor = true;
            this.nuevoBTN.Click += new System.EventHandler(this.nuevoBTN_Click);
            // 
            // FormPresupuestos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.confgBasicaTC);
            this.Name = "FormPresupuestos";
            this.Text = "FormPresupuestos";
            this.confgBasicaTC.ResumeLayout(false);
            this.ConfigTB.ResumeLayout(false);
            this.ConfigTB.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.AccesoriosTB.ResumeLayout(false);
            this.AccesoriosTB.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.GeneralTB.ResumeLayout(false);
            this.GeneralTB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl confgBasicaTC;
        private System.Windows.Forms.TabPage ConfigTB;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage AccesoriosTB;
        private System.Windows.Forms.TabPage GeneralTB;
        private System.Windows.Forms.TextBox precioProcesadorTB;
        private System.Windows.Forms.ComboBox preocesadorCB;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ListBox discoDuroLB;
        private System.Windows.Forms.ListBox memoriaLB;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton lujoRB;
        private System.Windows.Forms.RadioButton simpleRB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton color1RB;
        private System.Windows.Forms.RadioButton color14RB;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.CheckBox lectoraCDChB;
        private System.Windows.Forms.CheckBox LectoraDiscoChB;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton x80RB;
        private System.Windows.Forms.RadioButton x60RB;
        private System.Windows.Forms.RadioButton x40RB;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton m3RB;
        private System.Windows.Forms.RadioButton m2RB;
        private System.Windows.Forms.RadioButton m1RB;
        private System.Windows.Forms.CheckBox scanChB;
        private System.Windows.Forms.CheckBox impresChB;
        private System.Windows.Forms.TextBox lectoraCDTB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox lectoraDiscoTB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox ScannerPrecioTB;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox impresoraPrecioTB;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox direccionTB;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tlfTB;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox ClienteCB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button nuevoBTN;
        private System.Windows.Forms.Button calcBTN;
    }
}