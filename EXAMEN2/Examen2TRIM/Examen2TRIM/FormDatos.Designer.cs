﻿
namespace Examen2TRIM
{
    partial class FormDatos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.codLB = new System.Windows.Forms.ListBox();
            this.labelTitulo = new System.Windows.Forms.Label();
            this.InsertDatosGB = new System.Windows.Forms.GroupBox();
            this.codTB = new System.Windows.Forms.TextBox();
            this.precioTB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.AnnadirBTN = new System.Windows.Forms.Button();
            this.elimBTN = new System.Windows.Forms.Button();
            this.InsertDatosGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // codLB
            // 
            this.codLB.FormattingEnabled = true;
            this.codLB.Location = new System.Drawing.Point(36, 53);
            this.codLB.Name = "codLB";
            this.codLB.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.codLB.Size = new System.Drawing.Size(156, 316);
            this.codLB.TabIndex = 0;
            // 
            // labelTitulo
            // 
            this.labelTitulo.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock;
            this.labelTitulo.AutoSize = true;
            this.labelTitulo.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitulo.Location = new System.Drawing.Point(32, 18);
            this.labelTitulo.Name = "labelTitulo";
            this.labelTitulo.Size = new System.Drawing.Size(78, 22);
            this.labelTitulo.TabIndex = 1;
            this.labelTitulo.Text = "Códigos";
            // 
            // InsertDatosGB
            // 
            this.InsertDatosGB.Controls.Add(this.elimBTN);
            this.InsertDatosGB.Controls.Add(this.AnnadirBTN);
            this.InsertDatosGB.Controls.Add(this.label2);
            this.InsertDatosGB.Controls.Add(this.label1);
            this.InsertDatosGB.Controls.Add(this.precioTB);
            this.InsertDatosGB.Controls.Add(this.codTB);
            this.InsertDatosGB.Location = new System.Drawing.Point(292, 41);
            this.InsertDatosGB.Name = "InsertDatosGB";
            this.InsertDatosGB.Size = new System.Drawing.Size(210, 324);
            this.InsertDatosGB.TabIndex = 2;
            this.InsertDatosGB.TabStop = false;
            // 
            // codTB
            // 
            this.codTB.Location = new System.Drawing.Point(15, 56);
            this.codTB.Name = "codTB";
            this.codTB.Size = new System.Drawing.Size(144, 20);
            this.codTB.TabIndex = 0;
            // 
            // precioTB
            // 
            this.precioTB.Location = new System.Drawing.Point(15, 134);
            this.precioTB.Name = "precioTB";
            this.precioTB.Size = new System.Drawing.Size(144, 20);
            this.precioTB.TabIndex = 1;
            this.precioTB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.precioTB_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Código";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Precio";
            // 
            // AnnadirBTN
            // 
            this.AnnadirBTN.Location = new System.Drawing.Point(18, 203);
            this.AnnadirBTN.Name = "AnnadirBTN";
            this.AnnadirBTN.Size = new System.Drawing.Size(128, 31);
            this.AnnadirBTN.TabIndex = 4;
            this.AnnadirBTN.Text = "AÑADIR";
            this.AnnadirBTN.UseVisualStyleBackColor = true;
            this.AnnadirBTN.Click += new System.EventHandler(this.AnnadirBTN_Click);
            // 
            // elimBTN
            // 
            this.elimBTN.Location = new System.Drawing.Point(18, 240);
            this.elimBTN.Name = "elimBTN";
            this.elimBTN.Size = new System.Drawing.Size(128, 31);
            this.elimBTN.TabIndex = 5;
            this.elimBTN.Text = "ELIMINAR";
            this.elimBTN.UseVisualStyleBackColor = true;
            this.elimBTN.Click += new System.EventHandler(this.elimBTN_Click);
            // 
            // FormDatos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(608, 415);
            this.ControlBox = false;
            this.Controls.Add(this.InsertDatosGB);
            this.Controls.Add(this.labelTitulo);
            this.Controls.Add(this.codLB);
            this.Name = "FormDatos";
            this.Text = "FormDatos";
            this.InsertDatosGB.ResumeLayout(false);
            this.InsertDatosGB.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox codLB;
        private System.Windows.Forms.Label labelTitulo;
        private System.Windows.Forms.GroupBox InsertDatosGB;
        private System.Windows.Forms.Button elimBTN;
        private System.Windows.Forms.Button AnnadirBTN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox precioTB;
        private System.Windows.Forms.TextBox codTB;
    }
}