﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen2TRIM
{
    //Aqui tendremos los datos de nuestros clientes
    class Cliente
    {
        private String nombre;
        private String direccion;
        private int numTlf;


        //Cosntructor
        public Cliente() { }


        public Cliente(String nombre, String dir, int numTlf) {
            this.nombre = nombre;
            this.direccion = dir;
            this.numTlf = numTlf;
        }


        //SETTER Y GETTER
        public string NOMBRE { get => nombre; set => nombre = value; }
        public string DIRECCION { get => direccion; set => direccion = value; }
        public int NUMTLF { get => numTlf; set => numTlf = value; }
    }
}
