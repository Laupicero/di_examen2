﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen2TRIM
{
    public partial class FormPresupuestos : Form
    {
        private Cliente[] clietes;
        private Componentes[] componentes;
        
        public FormPresupuestos()
        {
            InitializeComponent();

            //Rellenar Array de clientes y componentes
            rellenarClientes();
            rellenarComponentes();

            //Preparación- relleno de datos
            rellenarComboProcesador();
            rellenarcomboboxCliente();
            rellenarListboxMemoria();
            rellenarListboxDiscoduro();
            rellenarListboxImpresora();
            rellenarListboxExamen();
        }

        


        //-----------------------
        // BOTONES calcularFM
        //-----------------------


        //Botón para calcular
        private void calcBTN_Click(object sender, EventArgs e)
        {

        }

        //Botón nuevo
        private void nuevoBTN_Click(object sender, EventArgs e)
        {

        }


        //Métodos auxiliares
        private void rellenarcomboboxCliente()
        {
            throw new NotImplementedException();
        }

        private void rellenarListboxImpresora()
        {
            throw new NotImplementedException();
        }

        private void rellenarListboxExamen()
        {
            throw new NotImplementedException();
        }

        private void rellenarListboxDiscoduro()
        {
            throw new NotImplementedException();
        }

        private void rellenarListboxMemoria()
        {
            throw new NotImplementedException();
        }

        private void rellenarComboProcesador()
        {

        }

        //Rellenar arrays
        private void rellenarComponentes()
        {
            //this.componentes
        }

        private void rellenarClientes()
        {
            this.clietes = new Cliente[18]{
                new Cliente ("Aragón Troncoso, Antonio Jose", "Pedrera Baja, 10 B", 926253550),
                new Cliente ("Berraquero Ruiz, David", "C/ Corregidor Vicente Cano Altares, 1- Bajo", 968202300),
                new Cliente ("Cárdenas Ortega, Juan Manuel", "Pintor Miro Mainou, 23-Local 1 (Lomo Frailes-Tamaracite)", 928250850),
                new Cliente ("De la Barrera Lora, Israel", "Plataforma Logistica Plaza C/ Tarento,Nave 23", 976551543),
                new Cliente ("Del Real López, Natán", "Av Salgueiro Maia, 1072 A/B", 217620300),
                new Cliente ("Díaz González, José María", "C/ Porvera 5 Planta 4ª", 913304000),
                new Cliente ("Domínguez Sánchez, Celia", "C/ Canillas, 83 (Local)", 914160014),
                new Cliente ("Heredia Torres, Alberto", "Plaza Cristo Rey, 1", 915531178),
                new Cliente ("Llamas Muñoz, José Manuel", "C/ Idioma Esperanto, 20", 913063600),
                new Cliente ("López Segovia, Manuel", "C/ Viriato, 27 - Local Comercial", 914476553),
                new Cliente ("López Torres, Andres", "C/ Vilamari, 82", 933252447),
                new Cliente ("Lucena Buendía, Laura", "BALMES,8-PLT.3ª DESPACHO 6 EDF.BALMES", 936929595),
                new Cliente ("Martínez Tirado, José Antonio", "C/ Sant Pau, 153", 972671763),
                new Cliente ("Pérez Nieves, María Eva", "Pol.Can Valero, Naves Poima A-25/26", 952878769),
                new Cliente ("Polo Fernández, Rafael", "Calle San Francisco de Paula, 2", 971760400),
                new Cliente ("Prieto Moreno, José Ramón", "C/ Gaspar Garcia Laviana, 3, Bajo", 950151727),
                new Cliente ("Real Chía, Antonio Manuel", "C/ Los Olmos, 5-7 Bajo", 985208219),
                new Cliente ("Sánchez Ortega, Borja", "Ctra. Ureda, 56 - Local", 923225099)
            };
        }

    }
}
